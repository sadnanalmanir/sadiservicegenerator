package ca.unbsj.cbakerlab.sadi.services;

@SuppressWarnings("unused")
private static final class Vocab
{
	private static Model m_model = ModelFactory.createDefaultModel();
		
	public static final Property type = m_model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
	public static final Resource MIRO_30000006 = m_model.createResource("http://purl.obolibrary.org/obo/MIRO_30000006");

}

