package ca.unbsj.cbakerlab.sadi.params;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.coode.owlapi.manchesterowlsyntax.ManchesterOWLSyntaxClassExpressionParser;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLImportsDeclaration;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import uk.ac.manchester.cs.owl.owlapi.mansyntaxrenderer.ManchesterOWLSyntaxOWLObjectRendererImpl;

public class ServiceParamExtractor {

    private static final Logger log = Logger.getLogger(ServiceParamExtractor.class);
    private static final String DESCRIPTION_VALIDATOR_FILE_NAME = "SADI_service_description_validator.jar";
    public static final String DOMAIN_ONTOLOGY_TPTP_SYNTAX_PATH = "domain-ontology/foftptp/";

    private String serviceOntologyURI;
    private String inputClassURI;
    private String outputClassURI;

    public ServiceParamExtractor(String serviceOntologyURI) {
        this.serviceOntologyURI = serviceOntologyURI;
    }

    public void init(boolean runDocumentResetter,
            boolean runTokenizer,
            boolean runSentenceSplitter,
            boolean runPosTagger,
            boolean runNpChunker,
            boolean runChemicalExtractor,
            boolean runDiseaseExtractor,
            boolean runCooccurrenceExtractor
    ) {

    }

    public boolean isValidURI(String targetUrl) {

        HttpURLConnection httpUrlConn;
        try {
            httpUrlConn = (HttpURLConnection) new URL(targetUrl)
                    .openConnection();
            httpUrlConn.setRequestMethod("HEAD");

            // Set timeouts in milliseconds
            httpUrlConn.setConnectTimeout(30000);
            httpUrlConn.setReadTimeout(30000);

            // Print HTTP status code/message for your information.
            //System.out.println("Response Code: " + httpUrlConn.getResponseCode());
            //System.out.println("Response Message: " + httpUrlConn.getResponseMessage());
            return (httpUrlConn.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return false;
        }
    }

    public String getInputURIFromServiceOntology(String serviceOntologyURI) {

        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLDataFactory df = OWLManager.getOWLDataFactory();
        IRI ontologyIRI = IRI.create(serviceOntologyURI);

        try {
            OWLOntology ontology = manager.loadOntologyFromOntologyDocument(ontologyIRI);
            // get all classes from the ontology
            Set<OWLClass> classes = ontology.getClassesInSignature();
            for (OWLClass cls : classes) {
                if (cls.getIRI().getFragment().contains("Input") || cls.getIRI().getFragment().contains("input")) {
                    this.inputClassURI = cls.getIRI().toString();
                }
            }
            manager.removeOntology(ontology);
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }
        return inputClassURI;
    }

    public String getOutputURIFromServiceOntology(String serviceOntologyURI) {

        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLDataFactory df = OWLManager.getOWLDataFactory();
        IRI ontologyIRI = IRI.create(serviceOntologyURI);

        try {
            OWLOntology ontology = manager.loadOntologyFromOntologyDocument(ontologyIRI);
            // get all classes from the ontology
            Set<OWLClass> classes = ontology.getClassesInSignature();
            for (OWLClass cls : classes) {
                if (cls.getIRI().getFragment().contains("Output") || cls.getIRI().getFragment().contains("output")) {
                    this.outputClassURI = cls.getIRI().toString();
                }
            }
            manager.removeOntology(ontology);
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }
        return outputClassURI;
    }

    public boolean isInputHYDRACompatible(String inputClassURI) {

        String sadiDescriptionValidatorPath = "";

        try {
            sadiDescriptionValidatorPath = new File(log.getClass().getResource("/" + DESCRIPTION_VALIDATOR_FILE_NAME).toURI()).getPath();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        String command = "java -cp " + sadiDescriptionValidatorPath + " com.ipsnp.hydra.sadi.ServiceDescriptionCheckShell --single-service-ontology-model true --input " + inputClassURI;
        log.info("-- Command begins --");
        log.info(command);
        log.info("-- Command ends   --");
        String inputValidationResult = executeCommand(command);
        log.info("-- Validation Result begins --");
        log.info(inputValidationResult);
        log.info("-- Validation Result ends --");
        
        if (!(StringUtils.contains(inputValidationResult, "Undefined nonterminal in input class")
                || StringUtils.contains(inputValidationResult, "Cannot retrieve the definition for input class"))) {
            return true;
        }

        return false;
    }

    public boolean isOutputHYDRACompatible(String outputClassURI) {

        String sadiDescriptionValidatorPath = "";

        try {
            sadiDescriptionValidatorPath = new File(log.getClass().getResource("/" + DESCRIPTION_VALIDATOR_FILE_NAME).toURI()).getPath();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        String command = "java -cp " + sadiDescriptionValidatorPath + " com.ipsnp.hydra.sadi.ServiceDescriptionCheckShell --single-service-ontology-model true" + " --output " + outputClassURI;
        log.info("-- Command begins --");
        log.info(command);
        log.info("-- Command ends   --");
        String outputValidationResult = executeCommand(command);
        log.info("-- Validation Result begins --");
        log.info(outputValidationResult);
        log.info("-- Validation Result ends --");

        if (!(StringUtils.contains(outputValidationResult, "Undefined nonterminal in output class")
                || StringUtils.contains(outputValidationResult, "Cannot retrieve the definition for output class"))) {
            return true;
        }

        return false;
    }

    private static String executeCommand(String command) {

        StringBuffer output = new StringBuffer();

        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader
                    = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();

    }

    public OWLClassExpression getInputClassExpr(String serviceOntologyURI, String inputClassURI) {

        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLDataFactory df = OWLManager.getOWLDataFactory();
        IRI ontologyIRI = IRI.create(serviceOntologyURI);
        ManchesterOWLSyntaxOWLObjectRendererImpl r = new ManchesterOWLSyntaxOWLObjectRendererImpl();
        ManchesterOWLSyntaxClassExpressionParser parser = new ManchesterOWLSyntaxClassExpressionParser(df, null);
        OWLClassExpression inputClassExpr = null;
        String owlInputClassExpr = "";
        try {
            OWLOntology ontology = manager.loadOntologyFromOntologyDocument(ontologyIRI);
            OWLClass inputClass = df.getOWLClass(IRI.create(inputClassURI));
            inputClassExpr = getClassExpr(ontology, inputClass);
            owlInputClassExpr = inputClassExpr.toString();
            //log.info(owlInputClassExpr);
            // Get OWL class expr as manchester expr string
            String inputManchesterClassExpr = getClassExprInManchesterSyntax(ontology, inputClass, r);
            //log.info(inputManchesterClassExpr);
            manager.removeOntology(ontology);
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }
        return inputClassExpr;
    }

    public OWLClassExpression getOutputClassExpr(String serviceOntologyURI, String outputClassURI) {

        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLDataFactory df = OWLManager.getOWLDataFactory();
        IRI ontologyIRI = IRI.create(serviceOntologyURI);
        OWLClassExpression outputClassExpr = null;
        String owlOutputClassExpr = "";
        try {
            OWLOntology ontology = manager.loadOntologyFromOntologyDocument(ontologyIRI);
            OWLClass outputClass = df.getOWLClass(IRI.create(outputClassURI));
            outputClassExpr = getClassExpr(ontology, outputClass);
            owlOutputClassExpr = outputClassExpr.toString();
            //log.info(owlOutputClassExpr);

            manager.removeOntology(ontology);
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }
        return outputClassExpr;
    }

    private OWLClassExpression getClassExpr(OWLOntology ontology, OWLClass owlClass) {
        OWLClassExpression owlClsExpr = null;

        if (isDefinedAsEquivalent(ontology, owlClass)) {
            for (OWLClassExpression eca : owlClass.getEquivalentClasses(ontology)) {
                owlClsExpr = eca;
            }
        } else if (isDefinedAsSubclass(ontology, owlClass)) {
            for (OWLClassExpression eca : owlClass.getSuperClasses(ontology)) {
                owlClsExpr = eca;
            }
        }
        return owlClsExpr;
    }

    private boolean isDefinedAsEquivalent(OWLOntology ontology, OWLClass owlClass) {
        if (owlClass.getEquivalentClasses(ontology).size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isDefinedAsSubclass(OWLOntology ontology, OWLClass owlClass) {
        if (owlClass.getSuperClasses(ontology).size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    private String getClassExprInManchesterSyntax(OWLOntology ontology, OWLClass cls, ManchesterOWLSyntaxOWLObjectRendererImpl r) {

        String manchesterOWLClsExpr = null;
        if (isDefinedAsEquivalent(ontology, cls)) {
            for (OWLClassExpression eca : cls.getEquivalentClasses(ontology)) {
                manchesterOWLClsExpr = r.render(eca);
            }
        } else if (isDefinedAsSubclass(ontology, cls)) {
            for (OWLClassExpression eca : cls.getSuperClasses(ontology)) {
                manchesterOWLClsExpr = r.render(eca);
            }
        }
        return manchesterOWLClsExpr;
    }

    public List<String> getImportedOntologies(String serviceOntologyURI) {

        List<String> domainOntologies = new ArrayList<String>();

        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLDataFactory df = OWLManager.getOWLDataFactory();
        IRI ontologyIRI = IRI.create(serviceOntologyURI);
        try {
            OWLOntology ontology = manager.loadOntologyFromOntologyDocument(ontologyIRI);
            Set<OWLImportsDeclaration> importedOntologies = ontology.getImportsDeclarations();
            for (OWLImportsDeclaration importedOnt : importedOntologies) {
                domainOntologies.add(importedOnt.getIRI().toString());
                log.info(importedOnt.getIRI().toString());
            }
            manager.removeOntology(ontology);
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }
        return domainOntologies;
    }

    public String getNameOfOntology(String OntologyURI) {
        String ontologyName = "";
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLDataFactory df = OWLManager.getOWLDataFactory();
        IRI ontologyIRI = IRI.create(OntologyURI);
        ontologyName = ontologyIRI.getFragment();
        return ontologyName;
    }

    public String translateOWLToTPTP(String domainOntology) {
        String owl2tptpLibPath = "./owl2tptpauxfiles/lib/";
        String parameterFilePath = "./owl2tptpauxfiles/paramfiles/";
        final String SIMPLE_PARAM_FILE_NAME = "sample_parameters.xml";
        final String NEW_PARAM_FILE_NAME = "new_parameters.xml";

        // add --cnf as option if you want conjunctive normal form instead of fof syntax         
        String command = "java -cp"
                + " "
                + owl2tptpLibPath + "owlapi-bin.jar:"
                + owl2tptpLibPath + "TptpParser.jar:"
                + owl2tptpLibPath + "getopt.jar:"
                + owl2tptpLibPath + "log4j.jar:"
                + owl2tptpLibPath + "owl2tptp.jar owl2tptp.OWL2TPTP"
                + " -p " + parameterFilePath + SIMPLE_PARAM_FILE_NAME
                + " -a " + parameterFilePath + NEW_PARAM_FILE_NAME
                + " -c " + domainOntology;

        //log.info(" Executing command to translate ");
        log.info("-- Command begins --");
        log.info(command);
        log.info("-- Command ends   --");
        
        String tptpResultsFromOWL = executeCommand(command);
        
        return tptpResultsFromOWL;
    }

    public String storeOntologyFofInTPTP(String domainOntologyName, String ontologyFofInTPTP) {

        BufferedWriter writer = null;
        String extention = "tptp";
        String pathToSave = "";

        try {
            String dirName = DOMAIN_ONTOLOGY_TPTP_SYNTAX_PATH;
            String fileName = domainOntologyName.replace(".owl", "") + "." + extention;
            
            File dir = new File(dirName);
            File file = new File(dir, domainOntologyName + "." + extention);
            pathToSave = DOMAIN_ONTOLOGY_TPTP_SYNTAX_PATH + domainOntologyName + "." + extention;
            org.apache.commons.io.FileUtils.writeStringToFile(file, ontologyFofInTPTP);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }
        return pathToSave;
    }

    String getServiceNameFromURI(String serviceOntologyURI) {
        String fragment = StringUtils.substringAfterLast(serviceOntologyURI, "/");
        String serviceName = StringUtils.substringBefore(fragment, ".owl");
        return serviceName;
    }

}
