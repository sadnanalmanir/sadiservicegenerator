package ca.unbsj.cbakerlab.sadi.params;


import ca.unbsj.cbakerlab.codegenerator.ServiceCodeGenerator;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Switch;
import org.semanticweb.owlapi.model.OWLClassExpression;

public class Main {

    private static final Logger log = Logger.getLogger(Main.class);

    static long corpusQueryTime = 0;

    public Main() {

    }

    /**
     * Executed from main. Supports JSAP parser to read arguments from command
     * line. Details of how to use arguments in Main please read in readme
     *
     * @param args arguments from command line
     * @throws Exception
     */
    public static void run(String args) {

        try {
            long time = System.currentTimeMillis();

            // Get command line arguments.
            String serviceOntologyURI = null;
            String serviceName = null;
            String inputClassURI = null;
            String outputClassURI = null;
            String email = null;
            String description = null;

            boolean needSeedInputData = false;
            boolean runGenerator = false;

            //
            // Define options.
            //
            {
                JSAP jsap = new JSAP();
                {
                    FlaggedOption s = new FlaggedOption("serviceOntologyURI").setStringParser(JSAP.STRING_PARSER).setLongFlag("Service Ontology URI").setRequired(true).setShortFlag('s');
                    s.setHelp("URI of the service ontology.");
                    jsap.registerParameter(s);
                }
                {
                    FlaggedOption s = new FlaggedOption("email").setStringParser(JSAP.STRING_PARSER).setLongFlag("email").setRequired(true).setShortFlag('e');
                    s.setHelp("The contact email of the author.");
                    jsap.registerParameter(s);
                }

                {
                    FlaggedOption s = new FlaggedOption("description").setStringParser(JSAP.STRING_PARSER).setLongFlag("description").setRequired(true).setShortFlag('d');
                    s.setHelp("The description of the sevice.");
                    jsap.registerParameter(s);
                }

                {
                    Switch s = new Switch("needSeedInputData").setLongFlag("seed");
                    s.setHelp("needSeedInputData: Service allPatients from a table do NOT need input values.");
                    jsap.registerParameter(s);
                }

                {
                    Switch s = new Switch("runGenerator").setLongFlag("generate");
                    s.setHelp("runGenerator");
                    jsap.registerParameter(s);
                }

                // Parse the command-line arguments.
                JSAPResult config = jsap.parse(args);

                // Display the helpful help in case trouble happens.
                if (!config.success()) {
                    log.info("==============================================");
                    log.info("= Problem with command line                  =");
                    log.info("==============================================");
                    System.err.println();
                    System.err.println(" " + jsap.getUsage());
                    System.err.println();
                    System.err.println(jsap.getHelp());

                    Iterator<?> messageIterator = config.getErrorMessageIterator();
                    while (messageIterator.hasNext()) {
                        System.err.println(messageIterator.next());
                    }
                    System.exit(1);
                }

                // Assign to variables.
                log.info("==============================================");
                log.info("=           Parameters assigned");
                log.info("==============================================");
                serviceOntologyURI = config.getString("serviceOntologyURI");
                if (serviceOntologyURI != null) {
                    log.info("Service Ontology URI: " + serviceOntologyURI);
                }

                email = config.getString("email");
                if (email != null) {
                    log.info("Contact email: " + email);
                }

                description = config.getString("description");
                if (description != null) {
                    log.info("Description: [Currently no space allowed, Use underscore instead.] " + description);
                }

                needSeedInputData = config.getBoolean("needSeedInputData");
                if (needSeedInputData) {
                    log.info("needSeedInputData [e.g. getXByY]: " + needSeedInputData);
                }
                // Modules.
                //
                runGenerator = config.getBoolean("runGenerator");
                if (runGenerator) {
                    log.info("runGenerator: " + runGenerator);
                }

            }
            
            

            //
            // RUN Generator.
            //	
            if (runGenerator) {

                ServiceParamExtractor extractor = new ServiceParamExtractor(serviceOntologyURI);

                serviceName = extractor.getServiceNameFromURI(serviceOntologyURI);
                
                if (!extractor.isValidURI(serviceOntologyURI)) {
                    //log.info("Invalid Service ontology :" + serviceOntologyURI);
                    throw new ServiceGenerationException("Invalid Service ontology :" + serviceOntologyURI);
                }
                inputClassURI = extractor.getInputURIFromServiceOntology(serviceOntologyURI);
                if (!extractor.isValidURI(inputClassURI)) {
                    //log.info("Invalid Service Input Class URI :" + inputClassURI);
                    throw new ServiceGenerationException("Invalid Service Input Class URI :" + inputClassURI);
                }
                outputClassURI = extractor.getOutputURIFromServiceOntology(serviceOntologyURI);
                if (!extractor.isValidURI(outputClassURI)) {
                    //log.info("Invalid Service Output Class URI :" + outputClassURI);
                    throw new ServiceGenerationException("Invalid Service Output Class URI :" + outputClassURI);
                }
                log.info("================================================");
                log.info("= Checking strict compatibility of Input class  ");
                log.info("================================================");
                if (!extractor.isInputHYDRACompatible(inputClassURI)) {
                    //log.info("Input incompatible with HYDRA:" + inputClassURI);		  	
                    throw new ServiceGenerationException("Input incompatible with HYDRA:" + inputClassURI);
                }
                log.info("================================================");
                log.info("= Checking strict compatibility of Output class  ");
                log.info("================================================");

                if (!extractor.isOutputHYDRACompatible(outputClassURI)) {
                    //log.info("Output incompatible with HYDRA:" + outputClassURI);
                    throw new ServiceGenerationException("Output incompatible with HYDRA:" + outputClassURI);
                }
                log.info("================================================");
                log.info("=       Getting Input class expression ");
                log.info("================================================");
                OWLClassExpression inputClassExpr = extractor.getInputClassExpr(serviceOntologyURI, inputClassURI);
                log.info(inputClassExpr);
                log.info("================================================");
                log.info("=       Getting Output class expression ");
                log.info("================================================");

                OWLClassExpression outputClassExpr = extractor.getOutputClassExpr(serviceOntologyURI, outputClassURI);
                log.info(outputClassExpr);
                log.info("=======================================================");
                log.info("= Getting ontologies imported in the Service ontology  ");
                log.info("=======================================================");

                List<String> domainOntologyURIS = extractor.getImportedOntologies(serviceOntologyURI);

                //String[] vals = {"--cnf",   "-c", "-p", "inputs/testsWithCourseOnt/sample_parameters.xml", "-a", "inputs/testsWithCourseOnt/new_parameters.xml", "-c", "http://cbakerlab.unbsj.ca:8080/haitohdemo/HAI.owl"};
                //String[] vals = {"-p", "inputs/testsWithCourseOnt/sample_parameters.xml", "-a", "inputs/testsWithCourseOnt/new_parameters.xml", "-c", "http://cbakerlab.unbsj.ca:8080/haitohdemo/HAI.owl"};
                //OWL2TPTP.convert(vals);
                log.info("===================================================================");
                log.info("= Translate and save imported domain ontologies into TPTP formulas ");
                log.info("====================================================================");

                for (String domainOntologyURI : domainOntologyURIS) {
                    if (!extractor.isValidURI(domainOntologyURI)) {
                        //log.info("Invalid Domain ontology :" + domainOntology);
                        throw new ServiceGenerationException("Invalid Domain ontology :" + domainOntologyURI);
                    } else {
                        String domainOntologyName = extractor.getNameOfOntology(domainOntologyURI);
                        log.info("Translation begins for : " + domainOntologyName);
                        String ontologyFofInTPTP = extractor.translateOWLToTPTP(domainOntologyURI);
                        log.info("Translation ends.");
                        log.info("\n");                        
                        String pathSaved = extractor.storeOntologyFofInTPTP(domainOntologyName, ontologyFofInTPTP);
                        log.info("Saved in : " + pathSaved);
                    }
                }

                /*
                 Properties pro = new Properties();
                 try {
                 pro.load(new FileInputStream(new File(log.getClass().getClassLoader().getResource("project.properties").toURI())));
                 } catch (FileNotFoundException e1) {
                 e1.printStackTrace();
                 } catch (IOException e1) {
                 e1.printStackTrace();
                 }

                 String gateHome = pro.getProperty("GATE_HOME");
                 //
                 // Init pipeline.
                 //
                 Pipeline pipeline = new Pipeline(gateHome);
                 pipeline.init(
                 true,
                 true,
                 true,
                 true,
                 true,
                 true,
                 true,
                 true
                 );
		
                 log.info("Processing Corpus Time " + (System.currentTimeMillis() - time) / 1000 + " seconds");
                
                 */
            }
            
            
            
            //ServiceCodeGenerator codeGenerator = new ServiceCodeGenerator(serviceOntologyURI, serviceName, inputClassURI, outputClassURI, email, description);
            //codeGenerator.execute();
            
            ServiceCodeGenerator codeGenerator = new ServiceCodeGenerator(serviceOntologyURI, serviceName, inputClassURI, outputClassURI, email, description);
            codeGenerator.generate();
            
            

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {

        args = new String[]{"-s ", "http://cbakerlab.unbsj.ca:8080/siema-ontologies/service-ontologies/allMosquitoPopulations.owl"," -e ", "sadnanalamnir@gmail.com", " -d ", "TEST_DESCRIPTION", " --seed", " --generate"};
        
        boolean hasArguments = false;
        if (args.length != 0) {
            if (args.length == 1 && args[0].equals("${args}")) {
                hasArguments = false;
            } else {
                hasArguments = true;
            }
        }

        if (hasArguments) {
            String arguments = Arrays.toString(args).replace(", ", " ");
            arguments = arguments.substring(1, arguments.length() - 1);
            log.info("==============================================");
            log.info("=      Arguments passed ");
            log.info("==============================================");
            log.info(arguments);
            run(arguments);
        } else {
            System.out.println("NO ARGUMENTS. CHECKARGUMENTS.");

        }
        System.out.println("\nAll done.");

    }
}
