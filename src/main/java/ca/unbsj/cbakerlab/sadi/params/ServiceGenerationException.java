package ca.unbsj.cbakerlab.sadi.params;

public class ServiceGenerationException extends Exception {

    public ServiceGenerationException() {
        super();
    }

    public ServiceGenerationException(String message) {
        super(message);
    }

    public ServiceGenerationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceGenerationException(Throwable cause) {
        super(cause);
    }
}
