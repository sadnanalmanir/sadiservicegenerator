/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.unbsj.cbakerlab.Utility;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 *
 * @author sadnana
 */
public class Utilities {

    private static Charset utf8 = StandardCharsets.UTF_8;

    /**
     * Read a URL into a string.
     *
     * @param url
     * @return
     * @throws IOException
     */
    public static String readFully(URL url) throws IOException {
        return readFully(url.openStream());
    }

    /**
     * Read an InputStream into a string.
     *
     * @param is
     * @return
     * @throws IOException
     */
    public static String readFully(InputStream is) throws IOException {
        BufferedReader input = new BufferedReader(new InputStreamReader(is));
        StringBuilder buf = new StringBuilder();
        String line;
        while ((line = input.readLine()) != null) {
            buf.append(line);
            buf.append("\n");
        }
        input.close();
        return buf.toString();
    }

    /**
     * Read a whole file as UTF-8
     *
     * @param filename
     * @return String
     * @throws IOException
     */
    public static String readWholeFileAsUTF8(String filename) throws IOException {

        InputStream in = new FileInputStream(filename);

        return readWholeFileAsUTF8(in);

    }

    /**
     * Read a whole stream as UTF-8
     *
     * @param in InputStream to be read
     * @return String
     * @throws IOException
     */
    public static String readWholeFileAsUTF8(InputStream in) throws IOException {

        Reader r = asUTF8(in);

        return readWholeFileAsUTF8(r);
    }

    /**
     * Create an unbuffered reader that uses UTF-8 encoding
     */
    static public Reader asUTF8(InputStream in) {
        return new InputStreamReader(in, utf8.newDecoder());
    }

    /**
     * Read a whole file as UTF-8
     *
     * @param r
     * @return String The whole file
     * @throws IOException
     */
    // Private worker as we are trying to force UTF-8. 
    private static String readWholeFileAsUTF8(Reader r) throws IOException {
        final int WHOLE_FILE_BUFFER_SIZE = 32 * 1024;        
        
        StringWriter sw = null;
        try {
            sw = new StringWriter(WHOLE_FILE_BUFFER_SIZE);

            char buff[] = new char[WHOLE_FILE_BUFFER_SIZE];
            for (;;) {
                int l = r.read(buff);
                if (l < 0) {
                    break;
                }
                sw.write(buff, 0, l);
            }
            return sw.toString();
        } finally {
            if (sw != null) {
                sw.close();
            }
        }

    }

}
