package ca.unbsj.cbakerlab.codegenerator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
import ca.unbsj.cbakerlab.codegenerator.HasModifableURI;
import ca.unbsj.cbakerlab.codegenerator.ServiceDescription;
/**
 * A simple class describing a SADI service.
 * @author Luke McCarthy
 */
public class ServiceBean implements Serializable, ServiceDescription, HasModifableURI
{
	private static final long serialVersionUID = 1L;
	
	private String URI;
	private String name;
	private String description;
	private String email;
	private String inputClassURI;
	private String inputClassLabel;
	private String outputClassURI;
	private String outputClassLabel;
	
	public ServiceBean()
	{
		URI = null;
		name = null;
		description = null;
		email = null;
		inputClassURI = null;
		inputClassLabel = "";
		outputClassURI = null;
		outputClassLabel = "";
	}
	
	/* (non-Javadoc)
	 * @see org.sadiframework.ServiceDescription#getURI()
	 */
	public String getURI()
	{
		return URI;
	}

	public void setURI(String URI)
	{
		this.URI = URI;
	}

	/* (non-Javadoc)
	 * @see org.sadiframework.ServiceDescription#getName()
	 */
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see org.sadiframework.ServiceDescription#getDescription()
	 */
	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	
	/* (non-Javadoc)
	 * @see org.sadiframework.ServiceDescription#getContactEmail()
	 */
	public String getContactEmail()
	{
		return email;
	}
	
	public void setContactEmail(String email)
	{
		this.email = email;
	}
		
        /* (non-Javadoc)
	 * @see org.sadiframework.ServiceDescription#getInputClassURI()
	 */
	public String getInputClassURI()
	{
		return inputClassURI;
	}

	public void setInputClassURI(String inputClassURI)
	{
		this.inputClassURI = inputClassURI;
	}

	/* (non-Javadoc)
	 * @see org.sadiframework.ServiceDescription#getInputClassLabel()
	 */
	public String getInputClassLabel()
	{
		return inputClassLabel;
	}

	public void setInputClassLabel(String inputClassLabel)
	{
		this.inputClassLabel = inputClassLabel;
	}

	/* (non-Javadoc)
	 * @see org.sadiframework.ServiceDescription#getOutputClassURI()
	 */
	public String getOutputClassURI()
	{
		return outputClassURI;
	}

	public void setOutputClassURI(String outputClassURI)
	{
		this.outputClassURI = outputClassURI;
	}

	/* (non-Javadoc)
	 * @see org.sadiframework.ServiceDescription#getOutputClassLabel()
	 */
	public String getOutputClassLabel()
	{
		return outputClassLabel;
	}

	public void setOutputClassLabel(String outputClassLabel)
	{
		this.outputClassLabel = outputClassLabel;
	}
	
	@Override
	public String toString()
	{
		StringBuilder buf = new StringBuilder();
		buf.append("{");
		buf.append("\n             URI: ");
		buf.append(getURI());
		buf.append("\n            Name: ");
		buf.append(getName());
		buf.append("\n     Description: ");
		buf.append(getDescription());
		buf.append("\n   Contact Email: ");
		buf.append(getContactEmail());
		buf.append("\n     Input Class: ");
		buf.append(getInputClassURI());
		buf.append(" (");
		buf.append(StringUtils.defaultIfEmpty(getInputClassLabel(), "no label"));
		buf.append(")");
		buf.append("\n    Output Class: ");
		buf.append(getOutputClassURI());
		buf.append(" (");
		buf.append(StringUtils.defaultIfEmpty(getOutputClassLabel(), "no label"));
		buf.append(")");
		buf.append("\n}");
		return buf.toString();
	}
}