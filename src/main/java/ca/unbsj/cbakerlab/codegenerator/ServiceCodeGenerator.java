/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.unbsj.cbakerlab.codegenerator;

import ca.unbsj.cbakerlab.Utility.Utilities;
import ca.unbsj.cbakerlab.sadi.params.ServiceGenerationException;
import ca.unbsj.cbakerlab.sadi.params.ServiceParamExtractor;
import ca.unbsj.cbakerlab.sqltemplate.querymanager.SAILQueryManager;
import ca.unbsj.cbakerlab.sqltemplate.schematicanswers.SchematicAnswersGenerator;
import static ca.unbsj.cbakerlab.sqltemplate.querymanager.sail.query_manager.SQLGenerator.tableColumns;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.regex.Pattern;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang.StringUtils;

import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author sadnana
 */
public class ServiceCodeGenerator {

    private static final Logger log = Logger.getLogger(ServiceCodeGenerator.class);
    private static final String SERVICE_PROPERTIES = "src/main/resources/sadi.properties";
    private static final String SOURCE_DIRECTORY = "src/main/java";
    private static final String WEB_XML_PATH = "src/main/webapp/WEB-INF/web.xml";
    private static final String INDEX_JSP_PATH = "src/main/webapp/index.jsp";

    private static final String AUTO_GENERATED_SERVICE_DIR = "autogen-sadiwebservice";
    private static final String DB_PROPERTY_FILENAME = "database";
    private static final String DB_CLASS_NAME = "MySqlDatabase";
    private static final String VOCAB_FILE_NAME = "Vocab";
    private static final String README_FILENAME = "README";
    private static final String SERVICE_PACKAGE_STRUCTURE = "ca.unbsj.cbakerlab.sadi.services";

    private static final boolean IS_SYNCHRONOUS_SERVICE = false;

    // Contents of the files after they are created from the skeletons
    private String webXMLContent;
    private String pomXMLContent;
    private String serviceClassContent;
    private String vocabClassContent;
    private String mysqlDBClassContent;
    private String dbPropertiesContent;
    private String indexJSPContent;

    private String serviceOntologyURI;

    private String serviceName;
    private static final String SERVICE_NAME_KEY = "serviceName"; // different than properties

    private String serviceClass;
    private static final String SERVICE_CLASS_KEY = "serviceClass";

    private String description;
    private String contactEmail;
    private String inputClassURI;
    private String outputClassURI;

    public ServiceCodeGenerator(String serviceOntologyURI, String serviceName, String inputClassURI, String outputClassURI, String contactEmail, String description) {
        this.serviceOntologyURI = serviceOntologyURI;
        this.serviceName = serviceName;
        this.serviceClass = SERVICE_PACKAGE_STRUCTURE + "." + serviceName;
        this.inputClassURI = inputClassURI;
        this.outputClassURI = outputClassURI;
        this.description = description;
        this.contactEmail = contactEmail;
    }

    public void generate() throws ServiceGenerationException {

        if (serviceName == null) {
            throw new ServiceGenerationException(String.format("missing required property %s", SERVICE_NAME_KEY));
        }
        if (serviceClass == null) {
            throw new ServiceGenerationException(String.format("missing required property %s", SERVICE_CLASS_KEY));
        }

        // setting service params
        ServiceBean bean = new ServiceBean();
        bean.setName(serviceName);
        bean.setInputClassURI(inputClassURI);
        bean.setOutputClassURI(outputClassURI);
        bean.setContactEmail(contactEmail);
        bean.setDescription(description);

        // Path for creating service codebase
        File basePath = new File(AUTO_GENERATED_SERVICE_DIR).getAbsoluteFile();

        // Clean all existing files in the same path exists
        if (basePath.exists()) {
            log.info("Cleaning all existing files in the base directory ...");
            if (basePath.listFiles().length > 0) {
                try {
                    // if there are files
                    FileUtils.cleanDirectory(basePath);
                    log.info("done.");
                } catch (IOException ex) {
                    java.util.logging.Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        log.info("=======================================================");
        log.info("==  Writing all source service files' in: " + basePath);
        log.info("=======================================================");

        // Writing pom.xml file
        String pomProjectName = StringUtils.substringBeforeLast(serviceClass, ".");;
        pomProjectName = StringUtils.substringAfterLast(pomProjectName, ".");

        File pomFile = new File(basePath, String.format("pom.xml"));

        try {
            log.info("Writing pom.xml ...");
            writePomFile(pomFile, pomProjectName);
            log.info("Finished.");
        } catch (Exception e) {
            String message = String.format("failed to write new pom.xml file for %s", serviceClass);
            log.error(message, e);
            throw new ServiceGenerationException(message);
        }

        // Writing connection class file for MySQL database
        String packagePath = StringUtils.substringBeforeLast(serviceClass, ".");
        String completeDBClassPath = packagePath.concat(".").concat(DB_CLASS_NAME);

        File dbConnClassFile = new File(basePath, String.format("%s/%s.java", SOURCE_DIRECTORY, completeDBClassPath.replace(".", "/")));

        try {
            log.info("Writing " + DB_CLASS_NAME + ".java ...");
            writeDBConnClassFile(dbConnClassFile, completeDBClassPath);
            log.info("Finished.");
        } catch (Exception e) {
            String message = String.format("failed to write new Database connection java file for %s", serviceClass);
            log.error(message, e);
            throw new ServiceGenerationException(message);
        }

        // Writing database.properties file in src/main/resources directory containing the access credentials
        String RESOURCES_DIRECTORY = StringUtils.substringBeforeLast(SERVICE_PROPERTIES, "/");
        File dbPropertiesFile = new File(basePath, String.format("%s/%s.properties", RESOURCES_DIRECTORY, DB_PROPERTY_FILENAME));

        try {
            log.info("Writing " + DB_PROPERTY_FILENAME + ".properties ...");
            writeDBPropertiesFile(dbPropertiesFile);
            log.info("Finished.");
        } catch (Exception e) {
            String message = String.format("failed to write new Database Properties file for %s", serviceClass);
            log.error(message, e);
            throw new ServiceGenerationException(message);
        }

        // Writinig README.txt file about the use of this tool
        File readmeFile = new File(basePath, String.format("%s.txt", README_FILENAME));

        try {
            log.info("Writing " + README_FILENAME + ".txt ...");
            writeReadmeFile(readmeFile);
            log.info("Finished.");
        } catch (Exception e) {
            String message = String.format("failed to write new README file for %s", serviceClass);
            log.error(message, e);
            throw new ServiceGenerationException(message);
        }

        // Writing web.xml file
        File webxmlPath = new File(basePath, WEB_XML_PATH);
        WebXmlParser webxml = new WebXmlParser();

        if (webxmlPath.exists()) {
            try {
                webxml.parse(webxmlPath);
            } catch (Exception e) {
                throw new ServiceGenerationException("failed to parse existing web.xml: " + e.getMessage());
            }
        }
        if (webxml.name2class.containsKey(serviceName)) {
            log.info(String.format("web.xml contains previous definition for servlet %s; it will be overwritten", serviceName));
        } else {
            log.info(String.format("adding servlet %s to web.xml", serviceName));
            webxml.name2class.put(serviceName, serviceClass);
            try {
                webxml.name2url.put(serviceName, String.format("/%s", URLEncoder.encode(serviceName, "UTF-8")));
            } catch (UnsupportedEncodingException e) {
                // this should never happen
                throw new ServiceGenerationException("failed to URL-encode service name: " + e.getMessage());
            }
        }
        try {
            log.info("Writing web.xml ...");
            writeWebXml(webxmlPath, webxml);
            log.info("Finished.");
        } catch (Exception e) {
            throw new ServiceGenerationException("failed to write web.xml", e);
        }

        // write index.jsp file
        try {
            log.info("Writing index.jsp ...");
            writeIndex(new File(basePath, INDEX_JSP_PATH), webxml.name2url);
            log.info("Finished.");
        } catch (Exception e) {
            throw new ServiceGenerationException("failed to write index.jsp", e);
        }

        // Writing Vocabulary class file         
        File vocabFile = new File(basePath, String.format("%s/%s.java", SOURCE_DIRECTORY, VOCAB_FILE_NAME));

        try {
            writeVocabFile(vocabFile, serviceClass, "inputClass", "outputClass");
        } catch (Exception e) {
            String message = String.format("failed to write new Vocab.java file for %s", serviceClass);
            log.error(message, e);
            throw new ServiceGenerationException(message);
        }

        // Writing service class file          
        File classFile = new File(basePath, String.format("%s/%s.java", SOURCE_DIRECTORY, serviceClass.replace(".", "/")));

        try {
            writeClassFile(classFile, serviceClass, "inputClass", "outputClass", bean, IS_SYNCHRONOUS_SERVICE, "select from where", "input.getResource()", "output.addLiteral()");
        } catch (Exception e) {
            String message = String.format("failed to write new java file for %s", serviceClass);
            log.error(message, e);
            throw new ServiceGenerationException(message);
        }

        /*
         SQL query generation from Vampire Prime engine
         */
        String tptpQuery
                = "include('rdb2ont.tptp').\n"
                + "\n"
                + "% HAI.owl ontology translated into tptp formulas without illegal tptp formula symbols \n"
                + "include('HAI_no_Illegal_Symbols.ontology.fof.tptp').\n"
                + "\n"
                + "% semantic query\n"
                + "input_clause(query4patOacisPID,conjecture,\n"
                + "  [\n"
                + "   --p_has_facility_description(Y, X),\n"
                + "    ++answer(X)\n"
                + "  ]).";

        SchematicAnswersGenerator schematicAnswersGenerator = new SchematicAnswersGenerator(tptpQuery);

        String schematicAnswers = schematicAnswersGenerator.generateSchematicAnswers();

        System.out.println("Schematic answers :\n" + schematicAnswers);
        
         SAILQueryManager sailQueryManager = new SAILQueryManager();
         String vpAutoGeneratedSQL = "";
         try {
         // the args are empty here, the callee method has the assigned arguments
         String[] args = {};
         vpAutoGeneratedSQL = sailQueryManager.generateSQLTemplate(args, schematicAnswers);
         } catch (Exception e) {
         e.printStackTrace();
         }
         /*
         // serialized table columns according to the output class descriptions ordering
         List<String> rdbTableColNames = new ArrayList<String>(tableColumns);
                       
         String refinedJavaSQL = refineSQL(vpAutoGeneratedSQL);
        
         System.out.println("--------------------------- Generated SQL Query ------------------------");
         System.out.println(refinedJavaSQL);
         System.out.println("-------------------------------------End--------------------------------");
         */

    } // end of generate()

    protected void writeClassFile(File classFile, String serviceClass, String inputClass, String outputClass, ServiceDescription serviceDescription, boolean async, String vpAutoGeneratedSQL, String inputCodeblock, String outputCodeBlock) throws Exception {
        /* create a copy of the service description where we can escape stuff...
         */
        ServiceBean escaped = new ServiceBean();
        escaped.setName(StringEscapeUtils.escapeJava(serviceDescription.getName()));
        escaped.setDescription(StringEscapeUtils.escapeJava(serviceDescription.getDescription()));
        escaped.setContactEmail(StringEscapeUtils.escapeJava(serviceDescription.getContactEmail()));
        escaped.setInputClassURI(serviceDescription.getInputClassURI());
        escaped.setOutputClassURI(serviceDescription.getOutputClassURI());

        createPath(classFile);
        FileWriter writer = new FileWriter(classFile);
        String template = Utilities.readFully(this.getClass().getResourceAsStream("/servicestub/templates/ServiceServletSkeleton"));
        VelocityContext context = new VelocityContext();
        context.put("description", escaped);
        context.put("package", StringUtils.substringBeforeLast(serviceClass, "."));
        context.put("class", StringUtils.substringAfterLast(serviceClass, "."));
        context.put("async", async);
        context.put("sqlquery", vpAutoGeneratedSQL);
        context.put("inputcodeblock", inputCodeblock);
        context.put("outputcodeblock", outputCodeBlock);
        Velocity.init();
        Velocity.evaluate(context, writer, "SADI", template);
        writer.close();
        System.out.println("==========================================");
        System.out.println("=========== service class ================");
        System.out.println("==========================================");
        serviceClassContent = org.apache.commons.io.FileUtils.readFileToString(classFile, "UTF-8");
        if (serviceClassContent.isEmpty()) {
            System.out.println("serviceClassName.java is empty.");
        } //else {
        //  System.out.println(serviceClassContent);
        //}
    }

    protected void writeVocabFile(File vocabFile, String serviceClass, String inputClass, String outputClass) throws Exception {
        /* create a copy of the service description where we can escape stuff...
         */

        //Set<String> classes = new HashSet<String>();
        //Set<String> properties = new HashSet<String>();
        Set<OWLIndividual> individuals = new HashSet<OWLIndividual>();
        Set<OWLClass> classes = new HashSet<OWLClass>();
        Set<OWLObjectProperty> objProps = new HashSet<OWLObjectProperty>();
        Set<OWLDataProperty> dataProps = new HashSet<OWLDataProperty>();

        ServiceParamExtractor extractor = new ServiceParamExtractor(serviceOntologyURI);
        OWLClassExpression inputClassExpr = extractor.getInputClassExpr(serviceOntologyURI, inputClassURI);
        OWLClassExpression outputClassExpr = extractor.getOutputClassExpr(serviceOntologyURI, outputClassURI);
        Set<OWLEntity> inEntities = inputClassExpr.getSignature();
        for (OWLEntity iEntity : inEntities) {
            if (iEntity.isOWLNamedIndividual()) {
                individuals.add(iEntity.asOWLNamedIndividual());
            }
            if (iEntity.isOWLClass()) {
                classes.add(iEntity.asOWLClass());
            }
            if (iEntity.isOWLObjectProperty()) {
                objProps.add(iEntity.asOWLObjectProperty());
            }
            if (iEntity.isOWLDataProperty()) {
                dataProps.add(iEntity.asOWLDataProperty());
            }
            //log.info("entity" + iEntity.toStringID());
        }

        Set<OWLEntity> outEntities = outputClassExpr.getSignature();
        for (OWLEntity oEntity : outEntities) {
            if (oEntity.isOWLNamedIndividual()) {
                individuals.add(oEntity.asOWLNamedIndividual());
            }
            if (oEntity.isOWLClass()) {
                classes.add(oEntity.asOWLClass());
            }
            if (oEntity.isOWLObjectProperty()) {
                objProps.add(oEntity.asOWLObjectProperty());
            }
            if (oEntity.isOWLDataProperty()) {
                dataProps.add(oEntity.asOWLDataProperty());
            }

            //log.info("entity" + oEntity.toStringID());
        }


        /* write the file...
         */
        createPath(vocabFile);
        FileWriter writer = new FileWriter(vocabFile);
        String template = Utilities.readFully(this.getClass().getResourceAsStream("/servicestub/templates/VocabularySkeleton"));
        VelocityContext context = new VelocityContext();
        context.put("package", StringUtils.substringBeforeLast(serviceClass, "."));
        context.put("dataproperties", dataProps);
        context.put("objproperties", objProps);
        context.put("classes", classes);
        context.put("individuals", individuals);
        Velocity.init();
        Velocity.evaluate(context, writer, "SADI", template);
        writer.close();
        System.out.println("==========================================");
        System.out.println("=========== Vocab class ================");
        System.out.println("==========================================");
        vocabClassContent = org.apache.commons.io.FileUtils.readFileToString(vocabFile, "UTF-8");
        if (vocabClassContent.isEmpty()) {
            System.out.println("Vocab.java is empty.");
        } //else {
        //  System.out.println(vocabClassContent);
        //}
    }

    private void writePomFile(File pomFile, String pomProjectName) throws Exception {
        /* write the file...
         */
        createPath(pomFile);
        FileWriter writer = new FileWriter(pomFile);
        String template = Utilities.readFully(this.getClass().getResourceAsStream("/servicestub/templates/pomSkeleton"));
        VelocityContext context = new VelocityContext();
        context.put("pomProjectName", pomProjectName);
        Velocity.init();
        Velocity.evaluate(context, writer, "SADI", template);
        writer.close();
        pomXMLContent = org.apache.commons.io.FileUtils.readFileToString(pomFile, "UTF-8");
        if (pomXMLContent.isEmpty()) {
            System.out.println("pom.xml is empty.");
        } //else {
        //    System.out.println(pomXMLContent);
        //}

    }

    protected void writeDBConnClassFile(File dbConnClassFile, String completeDBClassPath) throws Exception {

        /* write the file...
         */
        createPath(dbConnClassFile);
        FileWriter writer = new FileWriter(dbConnClassFile);
        String template = Utilities.readFully(this.getClass().getResourceAsStream("/servicestub/templates/DatabaseConnectionSkeleton"));
        VelocityContext context = new VelocityContext();
        context.put("package", StringUtils.substringBeforeLast(completeDBClassPath, "."));
        context.put("classname", StringUtils.substringAfterLast(completeDBClassPath, "."));
        Velocity.init();
        Velocity.evaluate(context, writer, "SADI", template);
        writer.close();
        mysqlDBClassContent = org.apache.commons.io.FileUtils.readFileToString(dbConnClassFile, "UTF-8");
        if (mysqlDBClassContent.isEmpty()) {
            System.out.println("MySqlDatabase.java is empty.");
        } //else {
        //    System.out.println(mysqlDBClassContent);
        //}
    }

    protected void writeDBPropertiesFile(File dbPropertiesFile) throws Exception {

        /* write the file...
         */
        createPath(dbPropertiesFile);
        FileWriter writer = new FileWriter(dbPropertiesFile);
        String template = Utilities.readFully(this.getClass().getResourceAsStream("/servicestub/templates/databasepropertiesSkeleton"));
        VelocityContext context = new VelocityContext();
        Velocity.init();
        Velocity.evaluate(context, writer, "SADI", template);
        writer.close();
        dbPropertiesContent = org.apache.commons.io.FileUtils.readFileToString(dbPropertiesFile, "UTF-8");
        if (dbPropertiesContent.isEmpty()) {
            System.out.println("database.properties is empty.");
        } //else {
        //    System.out.println(dbPropertiesContent);
        //}
    }

    protected void writeReadmeFile(File readmeFile) throws Exception {

        /* write the file...
         */
        createPath(readmeFile);
        FileWriter writer = new FileWriter(readmeFile);
        String template = Utilities.readFully(this.getClass().getResourceAsStream("/servicestub/templates/readmeSkeleton"));
        VelocityContext context = new VelocityContext();
        Velocity.init();
        Velocity.evaluate(context, writer, "SADI", template);
        writer.close();
    }

    private void writeWebXml(File webXml, WebXmlParser webxml) throws Exception {
        createPath(webXml);
        FileWriter writer = new FileWriter(webXml);
        String template = Utilities.readWholeFileAsUTF8(this.getClass().getResourceAsStream("/servicestub/templates/webXmlSkeleton"));
        VelocityContext context = new VelocityContext();
        context.put("name2class", webxml.name2class);
        context.put("name2url", webxml.name2url);
        Velocity.init();
        Velocity.evaluate(context, writer, "SADI", template);
        writer.close();
        webXMLContent = org.apache.commons.io.FileUtils.readFileToString(webXml, "UTF-8");
        if (webXMLContent.isEmpty()) {
            System.out.println("web.xml is empty.");
        } //else {
        //    System.out.println(webXMLContent);
        //}
    }

    private void writeIndex(File index, Map<String, String> name2url) throws Exception {
        createPath(index);
        FileWriter writer = new FileWriter(index);
        String template = Utilities.readWholeFileAsUTF8(this.getClass().getResourceAsStream("/servicestub/templates/indexSkeleton"));
        VelocityContext context = new VelocityContext();
        context.put("servlets", name2url);
        Velocity.init();
        Velocity.evaluate(context, writer, "SADI", template);
        //String webXMLContents = FileUtils.readWholeFileAsUTF8(index.getCanonicalPath());
        writer.close();
        // read contents of the web.xml file
        indexJSPContent = org.apache.commons.io.FileUtils.readFileToString(index, "UTF-8");
        if (indexJSPContent.isEmpty()) {
            System.out.println("index.jsp is empty.");
        } //else {
        //    System.out.println(indexJSPContent);
        //}
    }

    private static void createPath(File outfile) throws IOException {
        File parent = outfile.getParentFile();
        if (parent != null && !parent.isDirectory()) {
            if (!parent.mkdirs()) {
                throw new IOException(String.format("unable to create directory path ", parent));
            }
        }
    }

    private static class WebXmlParser extends DefaultHandler {

        Map<String, String> name2class;
        Map<String, String> name2url;

        public WebXmlParser() {
            name2class = new HashMap<String, String>();
            name2url = new HashMap<String, String>();
        }

        public void parse(File webxmlPath) throws Exception {
            SAXParserFactory spf = SAXParserFactory.newInstance();
            spf.setValidating(false);
            SAXParser sp = spf.newSAXParser();
            InputSource input = new InputSource(new FileReader(webxmlPath));
            input.setSystemId("file://" + webxmlPath.getAbsolutePath());
            sp.parse(input, this);
        }

        private StringBuffer accumulator = new StringBuffer();
        private String servletName;
        private String servletClass;
        private String servletUrl;

        @Override
        public void characters(char[] buffer, int start, int length) {
            accumulator.append(buffer, start, length);
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (localName.equals("servlet-name") || qName.equals("servlet-name")) {
                servletName = accumulator.toString().trim();
            } else if (localName.equals("servlet-class") || qName.equals("servlet-class")) {
                servletClass = accumulator.toString().trim();
            } else if (localName.equals("url-pattern") || qName.equals("url-pattern")) {
                servletUrl = accumulator.toString().trim();
            } else if (localName.equals("servlet") || qName.equals("servlet")) {
                name2class.put(servletName, servletClass);
            } else if (localName.equals("servlet-mapping") || qName.equals("servlet-mapping")) {
                name2url.put(servletName, servletUrl);
            }
            accumulator.setLength(0);
        }

    }

    private String refineSQL(String vpAutoGeneratedSQL) {
        String refinedSql = "";
        String line = "";

        Set<String> paramVars = new HashSet<String>();
        String[] arr = StringUtils.substringsBetween(vpAutoGeneratedSQL, "\"", "\"");
        // no duplicate parameter variables, 
        for (String param : arr) {
            paramVars.add(param);
        }

        for (String pv : paramVars) {
            vpAutoGeneratedSQL = StringUtils.replace(vpAutoGeneratedSQL, pv, "+" + pv + "+");
        }

        Scanner scanner = new Scanner(vpAutoGeneratedSQL);
        while (scanner.hasNextLine()) {
            line = scanner.nextLine();
            //if not the last line
            if (scanner.hasNextLine()) {
                if (line.length() > 0) {
                    refinedSql += "\n\t\t\t\"" + " " + line + "\"+";
                }
            } else //last line not requiring +
            {
                refinedSql += "\n\t\t\t\"" + " " + line + "\"";
            }
        }
        scanner.close();

        if (StringUtils.contains(refinedSql, "+\"\"+")) {
            refinedSql = StringUtils.replace(refinedSql, "+\"\"+", "+");
        }
		//refinedSql = vpAutoGeneratedSQL;

        return refinedSql;
    }

}
