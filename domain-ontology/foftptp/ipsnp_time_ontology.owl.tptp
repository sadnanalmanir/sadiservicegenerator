%  Common axioms:


  fof('owl:Thing U dataDomain cover all model elements',axiom,(
      ! [X] : 
        ( p_Thing(X)
        | dataDomain(X) ) )).



  fof('There is at least one individual',axiom,(
      ? [X] : p_Thing(X) )).



  fof('There is at least one data value',axiom,(
      ? [X] : dataDomain(X) )).



  fof('owl:Thing and dataDomain are disjoint',axiom,(
      ! [X] : ~ ( p_Thing(X)
        & dataDomain(X) ) )).



  fof('owl:Nothing is empty',axiom,(
      ! [X] : ~ p_Nothing(X) )).



  fof('integer literals are distinct from string-without-language literals',axiom,(
      ! [X,Y] : intLit(X) != strLit(Y) )).



  fof('integer literals are distinct from string-with-language literals',axiom,(
      ! [X,Y] : intLit(X) != strLitWLang(Y) )).



  fof('integer literals are distinct from general typed literals',axiom,(
      ! [X,Y] : intLit(X) != typedLit(Y) )).



  fof('string-without-language literals are distinct from string-with-language literals',axiom,(
      ! [X,Y] : strLit(X) != strLitWLang(Y) )).



  fof('string-without-language literals are distinct from general typed literals',axiom,(
      ! [X,Y] : strLit(X) != typedLit(Y) )).



  fof('string-with-language literals are distinct from general typed literals',axiom,(
      ! [X] : strLitWLang(X) != typedLit(Y) )).



  fof('intLit is a constructor',axiom,(
      ! [X,Y] : 
        ( intLit(X) = intLit(Y)
       => X = Y ) )).



  fof('stringLitNoLang is a constructor',axiom,(
      ! [X,Y] : 
        ( strLit(X) = strLit(Y)
       => X = Y ) )).



  fof('stringLitWithLang is a constructor',axiom,(
      ! [X,Y] : 
        ( strLitWLang(X) = strLitWLang(Y)
       => X = Y ) )).



  fof('typedLit is a constructor',axiom,(
      ! [X,Y] : 
        ( typedLit(X) = typedLit(Y)
       => X = Y ) )).



%  End of common axioms.


  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#time_region_has_boundary must be an individual',axiom,(
      ! [X,Y] : 
        ( p_time_region_has_boundary(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#time_region_has_boundary must be an individual',axiom,(
      ! [X,Y] : 
        ( p_time_region_has_boundary(X,Y)
       => p_Thing(Y) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#overlaps must be an individual',axiom,(
      ! [X,Y] : 
        ( p_overlaps(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#overlaps must be an individual',axiom,(
      ! [X,Y] : 
        ( p_overlaps(X,Y)
       => p_Thing(Y) ) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#time_region is a subclass of owl:Thing',axiom,(
      ! [X] : 
        ( p_time_region(X)
       => p_Thing(X) ) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#duration is an individual',axiom,(
      p_Thing(c_duration) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#finite_duration is an individual',axiom,(
      p_Thing(c_finite_duration) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#contains_or_equal must be an individual',axiom,(
      ! [X,Y] : 
        ( p_contains_or_equal(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#contains_or_equal must be an individual',axiom,(
      ! [X,Y] : 
        ( p_contains_or_equal(X,Y)
       => p_Thing(Y) ) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/open_class_classes.owl#time_specification is a subclass of owl:Thing',axiom,(
      ! [X] : 
        ( p_time_specification(X)
       => p_Thing(X) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#precedes must be an individual',axiom,(
      ! [X,Y] : 
        ( p_precedes(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#precedes must be an individual',axiom,(
      ! [X,Y] : 
        ( p_precedes(X,Y)
       => p_Thing(Y) ) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#finite_time_interval is a subclass of owl:Thing',axiom,(
      ! [X] : 
        ( p_finite_time_interval(X)
       => p_Thing(X) ) )).



  fof('Subject of http://www.w3.org/2002/07/owl#topDataProperty must be an individual',axiom,(
      ! [X,Y] : 
        ( p_topDataProperty(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://www.w3.org/2002/07/owl#topDataProperty must be a data value',axiom,(
      ! [X,Y] : 
        ( p_topDataProperty(X,Y)
       => dataDomain(Y) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#succeeds must be an individual',axiom,(
      ! [X,Y] : 
        ( p_succeeds(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#succeeds must be an individual',axiom,(
      ! [X,Y] : 
        ( p_succeeds(X,Y)
       => p_Thing(Y) ) )).



  fof('http://www.w3.org/2001/XMLSchema#duration is a datatype',axiom,(
      ! [X] : 
        ( p_duration(X)
       => dataDomain(X) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#contains must be an individual',axiom,(
      ! [X,Y] : 
        ( p_contains(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#contains must be an individual',axiom,(
      ! [X,Y] : 
        ( p_contains(X,Y)
       => p_Thing(Y) ) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/open_class_classes.owl#number is a subclass of owl:Thing',axiom,(
      ! [X] : 
        ( p_number(X)
       => p_Thing(X) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#meets must be an individual',axiom,(
      ! [X,Y] : 
        ( p_meets(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#meets must be an individual',axiom,(
      ! [X,Y] : 
        ( p_meets(X,Y)
       => p_Thing(Y) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_greater_than must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_greater_than(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_greater_than must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_greater_than(X,Y)
       => p_Thing(Y) ) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#duration is a subclass of owl:Thing',axiom,(
      ! [X] : 
        ( p_duration0(X)
       => p_Thing(X) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#time_region_has_end must be an individual',axiom,(
      ! [X,Y] : 
        ( p_time_region_has_end(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#time_region_has_end must be an individual',axiom,(
      ! [X,Y] : 
        ( p_time_region_has_end(X,Y)
       => p_Thing(Y) ) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#instant is a subclass of owl:Thing',axiom,(
      ! [X] : 
        ( p_instant(X)
       => p_Thing(X) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#print must be an individual',axiom,(
      ! [X,Y] : 
        ( p_print(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#print must be a data value',axiom,(
      ! [X,Y] : 
        ( p_print(X,Y)
       => dataDomain(Y) ) )).



  fof('http://www.w3.org/2001/XMLSchema#dateTime is a datatype',axiom,(
      ! [X] : 
        ( p_dateTime(X)
       => dataDomain(X) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_during_or_equal must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_during_or_equal(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_during_or_equal must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_during_or_equal(X,Y)
       => p_Thing(Y) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_after must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_after(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_after must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_after(X,Y)
       => p_Thing(Y) ) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#year is a subclass of owl:Thing',axiom,(
      ! [X] : 
        ( p_year(X)
       => p_Thing(X) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/closed_class_prop.owl#REPRESENTS_VALUE must be an individual',axiom,(
      ! [X,Y] : 
        ( p_REPRESENTS_VALUE(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/closed_class_prop.owl#REPRESENTS_VALUE must be a data value',axiom,(
      ! [X,Y] : 
        ( p_REPRESENTS_VALUE(X,Y)
       => dataDomain(Y) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_overlapped_by must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_overlapped_by(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_overlapped_by must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_overlapped_by(X,Y)
       => p_Thing(Y) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_started_by must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_started_by(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_started_by must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_started_by(X,Y)
       => p_Thing(Y) ) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#finite_time_interval is an individual',axiom,(
      p_Thing(c_finite_time_interval) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#infinity is an individual',axiom,(
      p_Thing(c_infinity) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#time_region is an individual',axiom,(
      p_Thing(c_time_region) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#intersects must be an individual',axiom,(
      ! [X,Y] : 
        ( p_intersects(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#intersects must be an individual',axiom,(
      ! [X,Y] : 
        ( p_intersects(X,Y)
       => p_Thing(Y) ) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#instant_or_infinity is an individual',axiom,(
      p_Thing(c_instant_or_infinity) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_before must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_before(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_before must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_before(X,Y)
       => p_Thing(Y) ) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#time_interval is an individual',axiom,(
      p_Thing(c_time_interval) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#infinity is a subclass of owl:Thing',axiom,(
      ! [X] : 
        ( p_infinity(X)
       => p_Thing(X) ) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#Gregorian_year_number is a subclass of owl:Thing',axiom,(
      ! [X] : 
        ( p_Gregorian_year_number(X)
       => p_Thing(X) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_finished_by must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_finished_by(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_finished_by must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_finished_by(X,Y)
       => p_Thing(Y) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_during must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_during(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_during must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_during(X,Y)
       => p_Thing(Y) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#time_interval_has_duration must be an individual',axiom,(
      ! [X,Y] : 
        ( p_time_interval_has_duration(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#time_interval_has_duration must be an individual',axiom,(
      ! [X,Y] : 
        ( p_time_interval_has_duration(X,Y)
       => p_Thing(Y) ) )).



  fof('http://www.w3.org/2001/XMLSchema#string is a datatype',axiom,(
      ! [X] : 
        ( p_string(X)
       => dataDomain(X) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#time_region_has_start must be an individual',axiom,(
      ! [X,Y] : 
        ( p_time_region_has_start(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#time_region_has_start must be an individual',axiom,(
      ! [X,Y] : 
        ( p_time_region_has_start(X,Y)
       => p_Thing(Y) ) )).



  fof('Subject of http://www.w3.org/2002/07/owl#topObjectProperty must be an individual',axiom,(
      ! [X,Y] : 
        ( p_topObjectProperty(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://www.w3.org/2002/07/owl#topObjectProperty must be an individual',axiom,(
      ! [X,Y] : 
        ( p_topObjectProperty(X,Y)
       => p_Thing(Y) ) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#time_interval is a subclass of owl:Thing',axiom,(
      ! [X] : 
        ( p_time_interval(X)
       => p_Thing(X) ) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#instant is an individual',axiom,(
      p_Thing(c_instant) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#year_number is a subclass of owl:Thing',axiom,(
      ! [X] : 
        ( p_year_number(X)
       => p_Thing(X) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_less_than_or_equal must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_less_than_or_equal(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_less_than_or_equal must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_less_than_or_equal(X,Y)
       => p_Thing(Y) ) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#finite_duration is a subclass of owl:Thing',axiom,(
      ! [X] : 
        ( p_finite_duration(X)
       => p_Thing(X) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#finishes must be an individual',axiom,(
      ! [X,Y] : 
        ( p_finishes(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#finishes must be an individual',axiom,(
      ! [X,Y] : 
        ( p_finishes(X,Y)
       => p_Thing(Y) ) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#Gregorian_year is a subclass of owl:Thing',axiom,(
      ! [X] : 
        ( p_Gregorian_year(X)
       => p_Thing(X) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/open_class_prop.owl#has_number must be an individual',axiom,(
      ! [X,Y] : 
        ( p_has_number(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/open_class_prop.owl#has_number must be an individual',axiom,(
      ! [X,Y] : 
        ( p_has_number(X,Y)
       => p_Thing(Y) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_met_by must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_met_by(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_met_by must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_met_by(X,Y)
       => p_Thing(Y) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_disjoint_with must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_disjoint_with(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_disjoint_with must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_disjoint_with(X,Y)
       => p_Thing(Y) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_greater_than_or_equal must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_greater_than_or_equal(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_greater_than_or_equal must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_greater_than_or_equal(X,Y)
       => p_Thing(Y) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_less_than must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_less_than(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#is_less_than must be an individual',axiom,(
      ! [X,Y] : 
        ( p_is_less_than(X,Y)
       => p_Thing(Y) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#has_Gregorian_number must be an individual',axiom,(
      ! [X,Y] : 
        ( p_has_Gregorian_number(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#has_Gregorian_number must be an individual',axiom,(
      ! [X,Y] : 
        ( p_has_Gregorian_number(X,Y)
       => p_Thing(Y) ) )).



  fof('Subject of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#starts must be an individual',axiom,(
      ! [X,Y] : 
        ( p_starts(X,Y)
       => p_Thing(X) ) )).



  fof('Object of http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#starts must be an individual',axiom,(
      ! [X,Y] : 
        ( p_starts(X,Y)
       => p_Thing(Y) ) )).



  fof('http://cbakerlab.unbsj.ca:8080/ont/ipsnp_time_ontology.owl#instant_or_infinity is a subclass of owl:Thing',axiom,(
      ! [X] : 
        ( p_instant_or_infinity(X)
       => p_Thing(X) ) )).



  fof('Class axiom #0',axiom,(
      ! [X] : 
        ( p_time_interval(X)
       => ( ? [Y0] : p_time_interval_has_duration(X,Y0)
          & ! [Y2,Z1] : 
              ( ( p_time_interval_has_duration(X,Z1)
                & p_time_interval_has_duration(X,Y2) )
             => Z1 = Y2 ) ) ) )).



  fof('Property axiom #1',axiom,(
      ! [X,Y] : 
        ( p_is_finished_by(X,Y)
      <=> p_finishes(Y,X) ) )).



  fof('Property axiom #2',axiom,(
      ! [X,Y] : 
        ( p_finishes(X,Y)
       => p_is_during(X,Y) ) )).



  fof('Property axiom #3',axiom,(
      ! [X] : ~ p_meets(X,X) )).



  fof('Property axiom #4',axiom,(
      ! [X] : ~ p_is_disjoint_with(X,X) )).



  fof('Class axiom #5',axiom,(
      ! [X] : 
        ( p_infinity(X)
       => p_instant_or_infinity(X) ) )).



  fof('Property axiom #6',axiom,(
      ! [X,Y] : 
        ( p_time_interval_has_duration(X,Y)
       => p_duration0(Y) ) )).



  fof('Class axiom #7',axiom,(
      ! [X] : 
        ( p_finite_time_interval(X)
       => p_time_interval(X) ) )).



  fof('Property axiom #8',axiom,(
      ! [X,Y] : 
        ( p_is_disjoint_with(X,Y)
       => p_topObjectProperty(X,Y) ) )).



  fof('Class axiom #9',axiom,(
      ! [X] : 
        ( p_instant(X)
      <=> ? [Y0] : 
            ( p_REPRESENTS_VALUE(X,Y0)
            & p_dateTime(Y0) ) ) )).



  fof('Property axiom #10',axiom,(
      ! [X,Y] : 
        ( p_is_started_by(X,Y)
      <=> p_starts(Y,X) ) )).



  fof('Property axiom #11',axiom,(
      ! [X,Y] : 
        ( p_is_met_by(X,Y)
       => p_succeeds(X,Y) ) )).



  fof('Class axiom #12',axiom,(
      ! [X] : 
        ( p_instant(X)
       => p_instant_or_infinity(X) ) )).



  fof('Property axiom #13',axiom,(
      ! [X] : ~ p_is_finished_by(X,X) )).



  fof('Property axiom #14',axiom,(
      ! [X,Y] : ~ ( p_is_before(X,Y)
        & p_meets(X,Y) ) )).



  fof('Property axiom #15',axiom,(
      ! [X,Y] : 
        ( p_meets(X,Y)
      <=> p_is_met_by(Y,X) ) )).



  fof('Property axiom #16',axiom,(
      ! [X] : ~ p_overlaps(X,X) )).



  fof('Property axiom #17',axiom,(
      ! [X,Y,Z] : 
        ( ( p_is_during_or_equal(X,Y)
          & p_is_during_or_equal(Y,Z) )
       => p_is_during_or_equal(X,Z) ) )).



  fof('Property axiom #18',axiom,(
      ! [X] : ~ p_is_before(X,X) )).



  fof('Property axiom #19',axiom,(
      ! [X,Y] : 
        ( p_precedes(X,Y)
       => p_topObjectProperty(X,Y) ) )).



  fof('Property axiom #20',axiom,(
      ! [X] : ~ p_starts(X,X) )).



  fof('Property axiom #21',axiom,(
      ! [X,Y] : 
        ( p_succeeds(X,Y)
       => p_time_region(X) ) )).



  fof('Class axiom #22',axiom,(
      ! [X] : 
        ( p_time_interval(X)
       => p_time_region(X) ) )).



  fof('Property axiom #23',axiom,(
      ! [X,Y] : 
        ( ( p_is_started_by(X,Y)
          & p_is_started_by(Y,X) )
       => X = Y ) )).



  fof('Individual axiom #24',axiom,(
      p_finite_duration(c_zero_duration) )).



  fof('Property axiom #25',axiom,(
      ! [X,Y] : 
        ( ( p_meets(X,Y)
          & p_meets(Y,X) )
       => X = Y ) )).



  fof('Property axiom #26',axiom,(
      ! [X,Y,Z] : 
        ( ( p_is_during(X,Y)
          & p_is_during(Y,Z) )
       => p_is_during(X,Z) ) )).



  fof('Property axiom #27',axiom,(
      ! [X,Y,Z] : 
        ( ( p_precedes(X,Y)
          & p_precedes(Y,Z) )
       => p_precedes(X,Z) ) )).



  fof('Property axiom #28',axiom,(
      ! [X,Y] : ~ ( p_precedes(X,Y)
        & p_succeeds(X,Y) ) )).



  fof('Property axiom #29',axiom,(
      ! [X] : ~ p_is_during(X,X) )).



  fof('Class axiom #30',axiom,(
      ! [X] : 
        ( p_finite_time_interval(X)
       => ( ? [Y0] : 
              ( p_time_region_has_end(X,Y0)
              & p_instant(Y0) )
          & ? [Y1] : 
              ( p_time_region_has_start(X,Y1)
              & p_instant(Y1) ) ) ) )).



  fof('Property axiom #31',axiom,(
      ! [X,Y] : 
        ( p_is_after(X,Y)
       => p_is_disjoint_with(X,Y) ) )).



  fof('Property axiom #32',axiom,(
      ! [X,Y] : 
        ( ( p_is_after(X,Y)
          & p_is_after(Y,X) )
       => X = Y ) )).



  fof('Property axiom #33',axiom,(
      ! [X,Y,Z] : 
        ( ( p_contains(X,Y)
          & p_contains(Y,Z) )
       => p_contains(X,Z) ) )).



  fof('Property axiom #34',axiom,(
      ! [X,Y] : 
        ( ( p_contains(X,Y)
          & p_contains(Y,X) )
       => X = Y ) )).



  fof('Property axiom #35',axiom,(
      ! [X,Y] : 
        ( p_contains(X,Y)
      <=> p_is_during(Y,X) ) )).



  fof('Individual axiom #36',axiom,(
      p_duration0(c_infinite_duration) )).



  fof('Property axiom #37',axiom,(
      ! [X] : ~ p_succeeds(X,X) )).



  fof('Property axiom #38',axiom,(
      ! [X] : ~ p_is_after(X,X) )).



  fof('Property axiom #39',axiom,(
      ! [X,Y] : 
        ( p_intersects(X,Y)
       => p_time_region(X) ) )).



  fof('Class axiom #40',axiom,(
      ! [X] : 
        ( p_time_region(X)
       => p_is_during_or_equal(X,c_whole_time_line) ) )).



  fof('Property axiom #41',axiom,(
      ! [X,Y] : 
        ( p_time_region_has_boundary(X,Y)
       => p_instant_or_infinity(Y) ) )).



  fof('Property axiom #42',axiom,(
      ! [X,Y] : 
        ( p_time_region_has_boundary(X,Y)
       => p_topObjectProperty(X,Y) ) )).



  fof('Property axiom #43',axiom,(
      ! [X,Y] : 
        ( p_time_region_has_end(X,Y)
       => p_time_region_has_boundary(X,Y) ) )).



  fof('Property axiom #44',axiom,(
      ! [X] : p_is_during_or_equal(X,X) )).



  fof('Property axiom #45',axiom,(
      ! [X] : ~ p_is_started_by(X,X) )).



  fof('Property axiom #46',axiom,(
      ! [X,Y] : 
        ( ( p_overlaps(X,Y)
          & p_overlaps(Y,X) )
       => X = Y ) )).



  fof('Property axiom #47',axiom,(
      ! [X,Y] : 
        ( p_time_interval_has_duration(X,Y)
       => p_time_interval(X) ) )).



  fof('Class axiom #48',axiom,(
      ! [X] : 
        ( p_finite_duration(X)
       => ? [Y0] : 
            ( p_REPRESENTS_VALUE(X,Y0)
            & p_duration(Y0) ) ) )).



  fof('Class axiom #49',axiom,(
      ! [X] : 
        ( p_instant(X)
      <=> ( p_time_interval(X)
          & p_time_interval_has_duration(X,c_zero_duration) ) ) )).



  fof('Class axiom #50',axiom,(
      ! [X] : 
        ( p_time_region(X)
       => p_time_specification(X) ) )).



  fof('Property axiom #51',axiom,(
      ! [X,Y] : 
        ( p_overlaps(X,Y)
       => p_intersects(X,Y) ) )).



  fof('Property axiom #52',axiom,(
      ! [X,Y] : 
        ( p_is_disjoint_with(X,Y)
       => p_is_disjoint_with(Y,X) ) )).



  fof('Property axiom #53',axiom,(
      ! [X,Y] : 
        ( p_is_finished_by(X,Y)
       => p_contains(X,Y) ) )).



  fof('Class axiom #54',axiom,(
      ! [X] : 
        ( p_instant_or_infinity(X)
       => p_finite_time_interval(X) ) )).



  fof('Class axiom #55',axiom,(
      ! [X] : 
        ( p_finite_time_interval(X)
      <=> ( p_time_interval(X)
          & ? [Y0] : 
              ( p_time_interval_has_duration(X,Y0)
              & p_finite_duration(Y0) ) ) ) )).



  fof('Property axiom #56',axiom,(
      ! [X,Y] : 
        ( p_is_greater_than_or_equal(X,Y)
      <=> p_is_less_than_or_equal(Y,X) ) )).



  fof('Property axiom #57',axiom,(
      ! [X] : ~ p_is_overlapped_by(X,X) )).



  fof('Property axiom #58',axiom,(
      ! [X,Y] : 
        ( p_is_before(X,Y)
       => p_precedes(X,Y) ) )).



  fof('Property axiom #59',axiom,(
      ! [X,Y] : 
        ( p_intersects(X,Y)
       => p_time_region(Y) ) )).



  fof('Property axiom #60',axiom,(
      ! [X,Y] : 
        ( p_has_Gregorian_number(X,Y)
       => p_has_number(X,Y) ) )).



  fof('Class axiom #61',axiom,(
      ! [X] : 
        ( p_Gregorian_year(X)
       => p_year(X) ) )).



  fof('Property axiom #62',axiom,(
      ! [X,Y] : 
        ( p_is_overlapped_by(X,Y)
       => p_intersects(X,Y) ) )).



  fof('Class axiom #63',axiom,(
      ! [X] : 
        ( p_year(X)
       => p_finite_time_interval(X) ) )).



  fof('Property axiom #64',axiom,(
      ! [X] : ~ p_time_interval_has_duration(X,X) )).



  fof('Class axiom #65',axiom,(
      ! [X] : 
        ( p_Gregorian_year_number(X)
       => p_year_number(X) ) )).



  fof('Property axiom #66',axiom,(
      ! [X,Y,Z] : 
        ( ( p_contains_or_equal(X,Y)
          & p_contains_or_equal(Y,Z) )
       => p_contains_or_equal(X,Z) ) )).



  fof('Property axiom #67',axiom,(
      ! [X,Y,Z] : 
        ( ( p_time_interval_has_duration(X,Y)
          & p_time_interval_has_duration(X,Z) )
       => Y = Z ) )).



  fof('Property axiom #68',axiom,(
      ! [X] : p_intersects(X,X) )).



  fof('Property axiom #69',axiom,(
      ! [X,Y] : 
        ( p_precedes(X,Y)
       => p_time_region(Y) ) )).



  fof('Property axiom #70',axiom,(
      ! [X,Y] : 
        ( ( p_finishes(X,Y)
          & p_finishes(Y,X) )
       => X = Y ) )).



  fof('Property axiom #71',axiom,(
      ! [X,Y] : 
        ( ( p_starts(X,Y)
          & p_starts(Y,X) )
       => X = Y ) )).



  fof('Property axiom #72',axiom,(
      ! [X,Y] : 
        ( p_starts(X,Y)
       => p_is_during(X,Y) ) )).



  fof('Property axiom #73',axiom,(
      ! [X,Y] : ~ ( p_is_after(X,Y)
        & p_is_met_by(X,Y) ) )).



  fof('Individual axiom #74',axiom,(
      p_time_region_has_end(c_whole_time_line,c_infinite_future) )).



  fof('Individual axiom #75',axiom,(
      p_time_region_has_start(c_whole_time_line,c_infinite_past) )).



  fof('Property axiom #76',axiom,(
      ! [X,Y] : 
        ( p_time_region_has_start(X,Y)
       => p_time_region_has_boundary(X,Y) ) )).



  fof('Property axiom #77',axiom,(
      ! [X,Y] : 
        ( p_succeeds(X,Y)
      <=> p_precedes(Y,X) ) )).



  fof('Property axiom #78',axiom,(
      ! [X,Y] : 
        ( p_contains(X,Y)
       => p_contains_or_equal(X,Y) ) )).



  fof('Class axiom #79',axiom,(
      ! [X] : ~ ( p_infinity(X)
        & p_instant(X) ) )).



  fof('Property axiom #80',axiom,(
      ! [X,Y] : 
        ( p_time_region_has_boundary(X,Y)
       => p_time_region(X) ) )).



  fof('Property axiom #81',axiom,(
      ! [X,Y] : 
        ( p_is_during_or_equal(X,Y)
       => p_intersects(X,Y) ) )).



  fof('Class axiom #82',axiom,(
      ! [X] : 
        ( p_time_region(X)
       => ? [Y0] : 
            ( p_print(X,Y0)
            & p_string(Y0) ) ) )).



  fof('Property axiom #83',axiom,(
      ! [X,Y] : 
        ( p_time_interval_has_duration(X,Y)
       => p_topObjectProperty(X,Y) ) )).



  fof('Class axiom #84',axiom,(
      ! [X] : 
        ( p_time_interval(X)
       => ( ? [Y0] : 
              ( p_time_region_has_end(X,Y0)
              & p_instant_or_infinity(Y0) )
          & ? [Y1] : 
              ( p_time_region_has_start(X,Y1)
              & p_instant_or_infinity(Y1) ) ) ) )).



  fof('Property axiom #85',axiom,(
      ! [X] : p_contains_or_equal(X,X) )).



  fof('Property axiom #86',axiom,(
      ! [X] : ~ p_precedes(X,X) )).



  fof('Property axiom #87',axiom,(
      ! [X,Y] : 
        ( p_is_after(X,Y)
       => p_succeeds(X,Y) ) )).



  fof('Property axiom #88',axiom,(
      ! [X,Y] : 
        ( ( p_is_before(X,Y)
          & p_is_before(Y,X) )
       => X = Y ) )).



  fof('Property axiom #89',axiom,(
      ! [X,Y] : 
        ( ( p_succeeds(X,Y)
          & p_succeeds(Y,X) )
       => X = Y ) )).



  fof('Property axiom #90',axiom,(
      ! [X,Y,Z] : 
        ( ( p_is_before(X,Y)
          & p_is_before(Y,Z) )
       => p_is_before(X,Z) ) )).



  fof('Property axiom #91',axiom,(
      ! [X,Y] : 
        ( p_meets(X,Y)
       => p_precedes(X,Y) ) )).



  fof('Property axiom #92',axiom,(
      ! [X,Y] : 
        ( ( p_precedes(X,Y)
          & p_precedes(Y,X) )
       => X = Y ) )).



  fof('Class axiom #93',axiom,(
      ! [X] : ~ ( p_finite_duration(X)
        & X = c_infinite_duration ) )).



  fof('Property axiom #94',axiom,(
      ! [X,Y] : 
        ( ( p_is_during(X,Y)
          & p_is_during(Y,X) )
       => X = Y ) )).



  fof('Class axiom #95',axiom,(
      ! [X] : 
        ( p_finite_duration(X)
       => p_duration0(X) ) )).



  fof('Property axiom #96',axiom,(
      ! [X,Y] : 
        ( p_is_during(X,Y)
       => p_is_during_or_equal(X,Y) ) )).



  fof('Individual axiom #97',axiom,(
      p_finite_duration(c_one_year_as_duration) )).



  fof('Property axiom #98',axiom,(
      ! [X,Y,Z] : 
        ( ( p_is_after(X,Y)
          & p_is_after(Y,Z) )
       => p_is_after(X,Z) ) )).



  fof('Property axiom #99',axiom,(
      ! [X,Y] : ~ ( p_intersects(X,Y)
        & p_is_disjoint_with(X,Y) ) )).



  fof('Individual axiom #100',axiom,(
      p_infinity(c_infinite_past) )).



  fof('Class axiom #101',axiom,(
      ! [X] : 
        ( p_year(X)
       => p_time_interval_has_duration(X,c_one_year_as_duration) ) )).



  fof('Property axiom #102',axiom,(
      ! [X] : ~ p_contains(X,X) )).



  fof('Individual axiom #103',axiom,(
      p_REPRESENTS_VALUE(c_zero_duration,typedLit("P0Y0M0DT0H0M0.0S","http://www.w3.org/2001/XMLSchema#duration")) )).



  fof('Property axiom #104',axiom,(
      ! [X,Y] : 
        ( p_is_started_by(X,Y)
       => p_contains(X,Y) ) )).



  fof('Property axiom #105',axiom,(
      ! [X,Y,Z] : 
        ( ( p_succeeds(X,Y)
          & p_succeeds(Y,Z) )
       => p_succeeds(X,Z) ) )).



  fof('Property axiom #106',axiom,(
      ! [X,Y] : 
        ( p_is_after(X,Y)
      <=> p_is_before(Y,X) ) )).



  fof('Property axiom #107',axiom,(
      ! [X,Y] : 
        ( ( p_is_overlapped_by(X,Y)
          & p_is_overlapped_by(Y,X) )
       => X = Y ) )).



  fof('Property axiom #108',axiom,(
      ! [X] : ~ p_finishes(X,X) )).



  fof('Property axiom #109',axiom,(
      ! [X,Y] : 
        ( p_precedes(X,Y)
       => p_time_region(X) ) )).



  fof('Property axiom #110',axiom,(
      ! [X,Y] : 
        ( p_contains_or_equal(X,Y)
       => p_intersects(X,Y) ) )).



  fof('Property axiom #111',axiom,(
      ! [X,Y] : 
        ( ( p_is_finished_by(X,Y)
          & p_is_finished_by(Y,X) )
       => X = Y ) )).



  fof('Property axiom #112',axiom,(
      ! [X,Y] : 
        ( p_is_greater_than(X,Y)
      <=> p_is_less_than(Y,X) ) )).



  fof('Individual axiom #113',axiom,(
      p_REPRESENTS_VALUE(c_one_year_as_duration,strLit("P1Y0M0DT0H0M0.0S")) )).



  fof('Property axiom #114',axiom,(
      ! [X,Y] : 
        ( ( p_is_met_by(X,Y)
          & p_is_met_by(Y,X) )
       => X = Y ) )).



  fof('Property axiom #115',axiom,(
      ! [X,Y] : 
        ( p_intersects(X,Y)
       => p_intersects(Y,X) ) )).



  fof('Property axiom #116',axiom,(
      ! [X,Y] : 
        ( ( p_time_interval_has_duration(X,Y)
          & p_time_interval_has_duration(Y,X) )
       => X = Y ) )).



  fof('Property axiom #117',axiom,(
      ! [X,Y] : 
        ( p_succeeds(X,Y)
       => p_topObjectProperty(X,Y) ) )).



  fof('Property axiom #118',axiom,(
      ! [X,Y] : 
        ( p_overlaps(X,Y)
      <=> p_is_overlapped_by(Y,X) ) )).



  fof('Property axiom #119',axiom,(
      ! [X,Y] : 
        ( p_intersects(X,Y)
       => p_topObjectProperty(X,Y) ) )).



  fof('Property axiom #120',axiom,(
      ! [X] : ~ p_is_met_by(X,X) )).



  fof('Property axiom #121',axiom,(
      ! [X,Y] : 
        ( p_succeeds(X,Y)
       => p_time_region(Y) ) )).



  fof('Property axiom #122',axiom,(
      ! [X,Y] : 
        ( p_is_before(X,Y)
       => p_is_disjoint_with(X,Y) ) )).



  fof('Individual axiom #123',axiom,(
      p_infinity(c_infinite_future) )).



  fof('Property axiom #124',axiom,(
      ! [X,Y] : 
        ( p_contains_or_equal(X,Y)
      <=> p_is_during_or_equal(Y,X) ) )).



  fof('Class axiom #125',axiom,(
      ! [X] : 
        ( p_year_number(X)
       => p_number(X) ) )).




